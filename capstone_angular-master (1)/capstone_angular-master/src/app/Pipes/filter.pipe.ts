import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any[],filterString:string,propertyName:string): any[] {
    const result:any = [];
    if(!value || filterString==='' || propertyName===''){
      return value;
    }
    value.forEach((data:any)=>{
      if(data[propertyName].trim().toLowerCase.include(filterString.toLowerCase)){
        result.push(data);
      }
    });
    return result;
  }
  

}
