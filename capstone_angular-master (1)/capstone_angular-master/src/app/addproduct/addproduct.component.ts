import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from '../models/Message';
import { Products } from '../models/Products';
import { ProductserviceService } from '../service/productservice.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {

  email:string = '';
  constructor(private serv:ProductserviceService,private router:Router) { }

  ngOnInit(): void {
  }

  isLoggedIn() {
    this.email = sessionStorage.getItem('email');
    if (this.email.endsWith("hcl.com")) {
      return true;
    } else {
      return false;
    }
  }

  // isError:boolean = false;
  // errMessage:string = '';
  // email:string = '';

  // registerProduct(data:any){
  //   let that =  this;
  //   this.email = data.email;
  //   let pro = new Products(data.productname,data.manu,data.price,data.desc,data.url,data.stock,data.cat);
  //   this.serv.addProduct(pro).subscribe({
  //     next(data:Message){
  //       console.log(data.message);
  //       alert("Product Added Successfully");
  //       that.router.navigate(['dashboard']);
  //     },
  //     error(data){
  //       alert("Product Already Exist");
  //       console.log(data.error);
  //       that.isError = true;
  //       that.errMessage = data.error.description;
  //     }
  //   })
  // }

  // isLoggedIn() {
  //   this.email = sessionStorage.getItem('email');
  //   if (this.email.endsWith("hcl.com")) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

}
