import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { ProductserviceService } from '../service/productservice.service';

@Component({
  selector: 'app-updateuser',
  templateUrl: './updateuser.component.html',
  styleUrls: ['./updateuser.component.css']
})
export class UpdateuserComponent implements OnInit {

  constructor(private serv:ProductserviceService,private router:Router) { }

  ngOnInit(): void {
  }

  isError:boolean = false;
  errMessage:string = '';
  email:string = '';

  registerUser(data:any){
    let that =  this;
    this.email = data.email;
    if(data.email !== null && (data.name !== null && data.password !== null) && (data.city !== null && data.mobile !== null)){
      let us = new LoginUser(data.email,data.name,data.password,data.city,data.mobile);
      this.serv.updateUser(us).subscribe({
      next(data:Message){
        console.log(data.message);
        alert('User Details updated');
        that.router.navigate(['/dashboard'])
      },
      error(data){
        alert("User Not Found");
        console.log(data.error);
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })
    }else{
      alert('Fiels can not be empty');
    }
    
  }

  logOut(){
    sessionStorage.removeItem('email');
    sessionStorage.clear();
    return true;
  }

}
