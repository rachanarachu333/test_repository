import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { ProductserviceService } from '../service/productservice.service';
import { } from '../models/ListOfUsers';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: LoginUser[] = [];
  mail: any;
  id: string;
  isError: boolean = false;
  errMessage: string = '';
  email: string = '';
  wantToDelete: boolean;
  constructor(private serv: ProductserviceService, private router: Router) {
    this.getUsers();
  }

  ngOnInit(): void {

  }

  getUsers() {
    this.serv.getUsers().subscribe((data: any) => {

      console.log(this.users);

      console.log(data);
      this.users = data;
    })
  }

  logOut() {
    sessionStorage.removeItem('email');
    sessionStorage.clear();
    return true;
  }

  getSingleUser() {
    if (this.mail == "") {
      this.getUsers();
    } else {
      this.users = this.users.filter(res => {
        return res.name.toLocaleLowerCase().match(this.mail.toLocaleLowerCase());
      });
    }
  }

  deleteUser(data: any) {
    console.log(data);
    this.wantToDelete = confirm("Are You Sure You Want to Delete The User");
    if (this.wantToDelete) {
      let that = this;
      this.email = data.email;
      let us = new LoginUser(data.email, data.name, data.password, data.city, data.mobile);
      this.serv.deleteUser(us).subscribe({
        next(data: Message) {
          console.log(data.message);
          alert("User Deleted Successfully");
          that.router.navigate(['/dashboard']);
          // that.getUsers();

        },
        error(data) {
          alert("User Not Found");
          console.log('failed');
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })
    } else {
      this.ngOnInit();
    }
    // this.ngOnInit();
  }

}


