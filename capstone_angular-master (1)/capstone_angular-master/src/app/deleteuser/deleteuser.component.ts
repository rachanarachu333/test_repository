import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { ProductserviceService } from '../service/productservice.service';

@Component({
  selector: 'app-deleteuser',
  templateUrl: './deleteuser.component.html',
  styleUrls: ['./deleteuser.component.css']
})
export class DeleteuserComponent implements OnInit {

  
  constructor(private serv: ProductserviceService, private router: Router) { }

  ngOnInit(): void {
  }

  id:string;
  isError: boolean = false;
  errMessage: string = '';
  email: string = '';

  deleteUser(data:any){
    let that = this;
    this.email = data.email;
    let us = new LoginUser(data.email,data.name,data.password,data.city,data.mobile);
    this.serv.deleteUser(us).subscribe({
      next(data: Message) {
        console.log(data.message);
        alert("User Deleted Successfully");
        that.router.navigate(['/dashboard']);
      },
      error(data) {
        alert("User Not Found");
        console.log('failed');
        console.log(data.error);
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })
  }

  isLoggedIn(){
    this.id = sessionStorage.getItem('email');
    if(this.id !== null){
      return false;
    }else{
      return true;
    }
  }

}
