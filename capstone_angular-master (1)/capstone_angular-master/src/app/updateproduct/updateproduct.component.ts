import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { Message } from '../models/Message';
import { Products } from '../models/Products';
import { ProductserviceService } from '../service/productservice.service';

@Component({
  selector: 'app-updateproduct',
  templateUrl: './updateproduct.component.html',
  styleUrls: ['./updateproduct.component.css']
})
export class UpdateproductComponent implements OnInit {


  email:String='';
  id:any;
  fileUrl:string;
  constructor(private serv:ProductserviceService,private router:Router,private activatedRoute:ActivatedRoute) {
    
  }

  ngOnInit(): void {
    console.log(this.activatedRoute.snapshot.params);
    this.id = this.activatedRoute.snapshot.params.id;
    console.log(this.id);
  }

  isLoggedIn() {
    this.email = sessionStorage.getItem('email');
    if (this.email.endsWith("hcl.com")) {
      return true;
    } else {
      return false;
    }
  }

  isError:boolean = false;
  errMessage:string = '';

  updateProduct(data:any){
      let that =  this;
    this.email = data.email;
    let pro = new Products(data.productid,data.productname,data.manu,data.price,data.desc,data.url,data.stock,data.cat);
    this.serv.updateProduct(pro).subscribe({
      next(data:Message){
        console.log(data.message);
        alert("Product Updated Successfully");
        that.router.navigate(['dashboard']);
      },
      error(data){
        alert("Product not Found");
        console.log(data.error);
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })
  }

  onImageSelect(data){

    if(data.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(data.target.files[0]);
      reader.onload = (ev:any)=>{
        this.fileUrl = ev.target.result
      }
      console.log(this.fileUrl);
    }

  }

  // registerProduct(data:any){
  //   let that =  this;
  //   this.email = data.email;
  //   let pro = new Products(data.productname,data.manu,data.price,data.desc,data.url,data.stock,data.cat);
  //   this.serv.updateProduct(pro).subscribe({
  //     next(data:Message){
  //       console.log(data.message);
  //       alert("Product Updated Successfully");
  //       that.router.navigate(['dashboard']);
  //     },
  //     error(data){
  //       alert("Product not Found");
  //       console.log(data.error);
  //       that.isError = true;
  //       that.errMessage = data.error.description;
  //     }
  //   })
  // }

  // isLoggedIn() {
  //   this.email = sessionStorage.getItem('email');
  //   if (this.email.endsWith("hcl.com")) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

}
