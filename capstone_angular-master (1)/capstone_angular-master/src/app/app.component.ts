import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'capstone';

  btnShowLogin:boolean = true;
  btnShowRegister:boolean = true;

  getLoginPage:boolean = false;
  getRegisterPage:boolean = false;

  getIndexPage:boolean = true;

  getTheLoginPage(){
    this.getIndexPage = false;
    return this.getIndexPage;
  }
}
