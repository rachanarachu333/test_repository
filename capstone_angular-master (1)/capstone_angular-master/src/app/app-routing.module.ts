import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {AdduserComponent} from './adduser/adduser.component';
import { AdminregisterComponent } from './adminregister/adminregister.component';
import { AdminloginUserComponent } from './adminlogin-user/adminlogin-user.component';
import { DeleteuserComponent } from './deleteuser/deleteuser.component';
import { UsersComponent } from './users/users.component';
import { UpdateuserComponent } from './updateuser/updateuser.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { UpdateproductComponent } from './updateproduct/updateproduct.component';

const routes: Routes = [
  {path:'',redirectTo:'login',pathMatch:'full'},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegisterComponent},
  {path:'dashboard',component:DashboardComponent},
  {path:'adduser',component:AdduserComponent},
  {path:'adminRegister',component:AdminregisterComponent},
  {path:'loginAdmin',component:AdminloginUserComponent},
  {path:'adduser',component:AdduserComponent},
  {path:'deleteuser',component:DeleteuserComponent},
  {path:'getUsers',component:UsersComponent},
  {path:'updateuser',component:UpdateuserComponent},
  {path:'addproduct',component:AddproductComponent},
  {path:'updateproduct/:id',component:UpdateproductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
