import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { ProductserviceService } from '../service/productservice.service';

@Component({
  selector: 'app-adminregister',
  templateUrl: './adminregister.component.html',
  styleUrls: ['./adminregister.component.css']
})
export class AdminregisterComponent implements OnInit {

  id: string;
  constructor(private serv: ProductserviceService, private router: Router) { }

  ngOnInit(): void {
  }

  isError: boolean = false;
  errMessage: string = '';
  email: string = '';

  registerAdmin(data: any) {

    let that = this;
    this.email = data.email;
    if (this.email.endsWith("hcl.com")) {
      let us = new LoginUser(data.email, data.name, data.password, data.city, data.mobile);
      this.serv.registerAdmin(us).subscribe({
        next(data: Message) {
          console.log(data.message);
          alert("You are registered Successfully");
          // sessionStorage.setItem('email',this.email);
          that.router.navigate(['/loginAdmin']);
        },
        error(data) {
          console.log(data.error);
          alert("You are registered Already");
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })
    } else {
      alert("You are not And Admin");
    }
  }
}
