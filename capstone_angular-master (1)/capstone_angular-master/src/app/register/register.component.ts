import { Message } from '../models/Message';
import { Component, OnInit } from '@angular/core';
import { LoginUser } from '../models/LoginUser';
import { ProductserviceService } from '../service/productservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private serv:ProductserviceService,private router:Router) { 

  }

  ngOnInit(): void {
    
  }

  isError:boolean = false;
  errMessage:string = '';
  email:string = '';

  registerUser(data:any){
    let that =  this;
    this.email = data.email;
    let us = new LoginUser(data.email,data.name,data.password,data.city,data.mobile);
    this.serv.checkLogin(us).subscribe({
      next(data:Message){
        console.log(data.message);
        alert("You Are Registered Successfully");
        that.router.navigate([''])
      },
      error(data){
        alert("User Already Registered");
        console.log(data.error);
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })
  }
}
