import { Message } from '../models/Message';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { ProductserviceService } from '../service/productservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isError: boolean = false;
  errMessage: string = '';
  email: string = '';

  constructor(private serv: ProductserviceService, private router: Router) { }

  ngOnInit(): void {
  }

  loginUser(data: any) {
    let that = this;
    this.email = data.email;
    let us = new LoginUser(data.email,data.name,data.password,data.city,data.mobile);
    this.serv.login(us).subscribe({
      next(data: Message) {
        console.log(data.message);
        sessionStorage.setItem('email', that.email);
        that.router.navigate(['/dashboard']);
      },
      error(data) {
        console.log('failed');
        console.log(data.error);
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })
  }


}
