import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ListOfProducts } from '../models/ListOfProducts';
import { ListOfUsers } from '../models/ListOfUsers';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { Products } from '../models/Products';
import { ProductserviceService } from '../service/productservice.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  constructor(private service: ProductserviceService,private router: Router) {
    this.getProducts();
  }


  email: string = '';
  users: LoginUser[] = [];
  products: Products[] = [];
  images:any = [];
  product: any;
  name:any;


  isError: boolean = false;
  errMessage: string = '';
  wantToDelete: boolean;


  ngOnInit(): void {
  }

  logOut() {
    sessionStorage.removeItem('email');
    sessionStorage.clear();
    return true;
  }

  isLoggedIn() {
    this.email = sessionStorage.getItem('email');
    if (this.email.endsWith("hcl.com")) {
      return true;
    } else {
      return false;
    }
  }

  getUsers() {
    this.service.getUsers().subscribe((data: any) => {

      console.log(this.users);

      console.log(data);
      this.users = data;
      let us = new ListOfUsers();
      us.setUsers = this.users;
    }
    )
  }


  getProducts() {
    let that = this;
    this.service.getProducts().subscribe((data: any) => {

      console.log(this.products);

      console.log(data);
      this.products = data;
      let us = new ListOfProducts();
      us.setProducts = this.products;
      console.log(us.getProducts);
      for(let val of this.products){
        console.log(val.img_url);
      }
    }
    )
  }

  getSingleProduct(){
    if (this.name == "") {
      this.getProducts();
    } else {
      this.products = this.products.filter(res => {
        return res.product_name.toLocaleLowerCase().match(this.name.toLocaleLowerCase());
      });
    }
  }

  deleteProduct(prod:any){

    console.log(prod);
    this.wantToDelete = confirm("Are You Sure You Want to Delete The User");
    if (this.wantToDelete) {
      let that = this;
      this.email = prod.email;
      let pro = new Products(prod.product_id,prod.product_name,prod.manufacturer,prod.price,prod.description,prod.img_url,prod.stock,prod.category);
      this.service.deleteProduct(pro).subscribe({
        next(data: Message) {
          console.log(data.message);
          alert("Product Deleted Successfully");
          that.router.navigate(['/dashboard']);

        },
        error(data) {
          alert("Product Not Found");
          console.log('failed');
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })
    } else {
      this.ngOnInit();
    }
    // this.ngOnInit();


  }

  updateProduct(data:any){



  }

}


