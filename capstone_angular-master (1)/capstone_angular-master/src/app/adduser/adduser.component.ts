import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { ProductserviceService } from '../service/productservice.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  constructor(private serv: ProductserviceService, private router: Router) { }

  ngOnInit(): void {
  }

  isError: boolean = false;
  errMessage: string = '';
  email: string = '';

  registerUser(data: any) {
    let that = this;
    this.email = data.email;
    if (this.email.endsWith("hcl.com")) {
      alert("Provide a User Email");
    } else {

      let us = new LoginUser(data.email, data.name, data.password, data.city, data.mobile);
      this.serv.checkLogin(us).subscribe({
        next(data: Message) {
          console.log(data.message);
          alert('User Add Successfully');
          that.router.navigate(['/dashboard'])
        },
        error(data) {
          alert("User Already Exist");
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })

    }
  }

  logOut() {
    sessionStorage.removeItem('email');
    sessionStorage.clear();
    return true;
  }
}
