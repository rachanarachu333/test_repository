import {Category} from '../models/Category';

export class Products{

    public product_id:number;
    public product_name:string;
    public manufacturer:string;
    public price:number;
    public description:string;
    public img_url:string;
    public stock:number;
    public category:number;
    

    constructor(product_id,product_name,manufacturer,price,description,img_url,stock,category){
        this.product_id = product_id;
        this.product_name = product_name;
        this.description = description;
        this.price = price;
        this.stock = stock;
        this.manufacturer = manufacturer;
        this.img_url = img_url;
        this.category = category;
    }

}