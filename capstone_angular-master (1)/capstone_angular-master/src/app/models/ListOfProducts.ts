import { Products } from './Products';

export class ListOfProducts{
    public listOfProducts:Products[];

    constructor(){
        
    }

    
    public set setProducts(list:Products[]) {
        this.listOfProducts = list;
    }
    

    public get getProducts() : Products[] {
        return this.listOfProducts;
    }
    
}