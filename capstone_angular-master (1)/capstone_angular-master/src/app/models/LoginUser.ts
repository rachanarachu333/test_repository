export class LoginUser{
    public email:string;
    public name:string;
    public password:string;
    public city:String;
    public mobile:String;

    constructor(email,name,password,city,mobile){
        this.email = email;
        this.name = name;
        this.password = password;
        this.city= city;
        this.mobile= mobile;
    }
}