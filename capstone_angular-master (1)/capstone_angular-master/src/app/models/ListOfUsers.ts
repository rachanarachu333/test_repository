import {LoginUser} from './LoginUser';

export class ListOfUsers{
    
    public listOfUsers:LoginUser[];

    constructor(){
        
    }

    public get getUsers() : LoginUser[]{
        return this.listOfUsers;
    }

    public set setUsers(list:LoginUser[]){
        this.listOfUsers = list;
    }
}