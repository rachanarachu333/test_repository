import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { Products } from '../models/Products';

@Injectable({
  providedIn: 'root'
})

export class ProductserviceService {
  
  constructor(private router:Router,private http:HttpClient) { }

  urlUser:string = 'http://localhost:8081/';
  urlAdmin:string = 'http://localhost:8082/';

  checkLogin(loginUser:LoginUser):Observable<Message>{

    return this.http.post<Message>(
      this.urlUser+'post',loginUser);
  }

  login(user:LoginUser):Observable<Message>{
    return this.http.post<Message>(
      this.urlUser+'loggedin',user);
  }


  // For Admin Registration
   loginAdmin(admin:LoginUser):Observable<Message>{
    return this.http.post<Message>(
      this.urlAdmin+'loginAdmin',admin);
  }


  // For Admin Registration
  registerAdmin(admin:LoginUser):Observable<Message>{
    return this.http.post<Message>(
      this.urlAdmin+'getRegister',admin);
  }

  deleteUser(user: LoginUser):Observable<Message>{
    return this.http.post<Message>(
      this.urlUser+'deleteuser',user);
  }

  getUsers():Observable<LoginUser[]>{

    return this.http.get<LoginUser[]>(
      this.urlUser+'getUsers'
    )
  }


  updateUser(loginUser:LoginUser):Observable<Message>{

    return this.http.post<Message>(
      this.urlUser+'updateuser',loginUser);
  }

  getProducts():Observable<Products[]>{

    return this.http.get<Products[]>(
      this.urlUser+'products'
    )
  }

  getImages():Observable<any>{

    return this.http.get(
      this.urlAdmin+'images'
    );
  }


  // product CRUD Operations

  addProduct(product:Products):Observable<Message>{

    return this.http.post<Message>(
      this.urlUser+'addproduct',product);
  }

  updateProduct(pro: Products):Observable<Message> {
    return this.http.post<Message>(
      this.urlUser+'updateproduct',pro
    );
  }

  deleteProduct(pro:Products):Observable<Message>{
    return this.http.post<Message>(
      this.urlUser+'deleteproduct',pro
    );
  }

}
