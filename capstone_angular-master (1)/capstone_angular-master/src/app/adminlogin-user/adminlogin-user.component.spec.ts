import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminloginUserComponent } from './adminlogin-user.component';

describe('AdminloginUserComponent', () => {
  let component: AdminloginUserComponent;
  let fixture: ComponentFixture<AdminloginUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminloginUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminloginUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
