import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { ProductserviceService } from '../service/productservice.service';

@Component({
  selector: 'app-adminlogin-user',
  templateUrl: './adminlogin-user.component.html',
  styleUrls: ['./adminlogin-user.component.css']
})
export class AdminloginUserComponent implements OnInit {

  isError: boolean = false;
  errMessage: string = '';
  email: string = '';
  emailId:string;

  constructor(private serv: ProductserviceService, private router: Router) { }

  ngOnInit(): void {
  }

  loginAdmin(data){
    let that = this;
    this.emailId = data.email;
    console.log(this.email);
    let us = new LoginUser(data.email,data.name,data.password,data.city,data.mobile);
    this.serv.loginAdmin(us).subscribe({
      next(data: Message) {
        console.log(data.message);
        sessionStorage.setItem("email", that.emailId);
        that.router.navigate(['/dashboard']);
      },
      error(data) {
        console.log('failed');
        console.log(data.error);
        alert('Login Failed Try After Sometime');
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })
  }

}
