package com.capstone.app.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class _Order{
	@Id
	@Column(columnDefinition = "int")
	private int order_id;

	@Column(columnDefinition = "varchar(10)")
	private	String orderDate;

	@Column(columnDefinition = "double")
	private double order_amount;

//	@ManyToMany(cascade=CascadeType.ALL)
//	@JoinColumn(name="product")
//	private List<Products> product=new ArrayList<>();

//	@ManyToOne
//	@JoinColumn(name = "loginusers")
//	private LoginUser loginusers;
	
	public _Order() {

	}

	public _Order(int order_Id,String orderDate, double order_amount) {
		super();
		this.order_id = order_Id;
		this.orderDate = orderDate;
		this.order_amount = order_amount;
	}

	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

//	public LoginUser getLoginusers() {
//		return loginusers;
//	}
//
//	public void setLoginusers(LoginUser loginusers) {
//		this.loginusers = loginusers;
//	}

	public int getOrder_Id() {
		return order_id;
	}

	public void setOrder_Id(int order_Id) {
		this.order_id = order_Id;
	}

	public  String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public double getOrder_amount() {
		return order_amount;
	}

	public void setOrder_amount(double order_amount) {
		this.order_amount = order_amount;
	}

//	public List<Products> getProduct() {
//		return product;
//	}
//
//	public void setProduct(List<Products> product) {
//		this.product = product;
//	}

	@Override
	public String toString() {
		return "Order [order_Id=" + order_id + ", orderDate=" + orderDate + ", order_amount=" + order_amount
				+"]";
	}


}//package com.capstone.app.entities;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToMany;
//
//@Entity
//public class _Order{
//	@Id
//	@Column(columnDefinition = "int")
//	private int order_id;
//
//	@Column(columnDefinition = "varchar(10)")
//	private	String orderDate;
//
//	@Column(columnDefinition = "double")
//	private double order_amount;
//
//	@ManyToMany(cascade=CascadeType.ALL)
//	@JoinColumn(name="product")
//	private List<Products> product=new ArrayList<>();
//
//
//	public _Order() {
//
//	}
//
//	public _Order(int order_Id,String orderDate, double order_amount, List<Products> product) {
//		super();
//		this.order_id = order_Id;
//		this.orderDate = orderDate;
//		this.order_amount = order_amount;
//		this.product = product;
//	}
//
//	public int getOrder_Id() {
//		return order_id;
//	}
//
//	public void setOrder_Id(int order_Id) {
//		this.order_id = order_Id;
//	}
//
//	public  String getOrderDate() {
//		return orderDate;
//	}
//
//	public void setOrderDate(String orderDate) {
//		this.orderDate = orderDate;
//	}
//
//	public double getOrder_amount() {
//		return order_amount;
//	}
//
//	public void setOrder_amount(double order_amount) {
//		this.order_amount = order_amount;
//	}
//
//	public List<Products> getProduct() {
//		return product;
//	}
//
//	public void setProduct(List<Products> product) {
//		this.product = product;
//	}
//
//	@Override
//	public String toString() {
//		return "Order [order_Id=" + order_id + ", orderDate=" + orderDate + ", order_amount=" + order_amount
//				+ ", product=" + product + "]";
//	}
//
//
//}