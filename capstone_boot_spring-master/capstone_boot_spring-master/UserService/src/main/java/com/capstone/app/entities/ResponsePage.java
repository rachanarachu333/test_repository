package com.capstone.app.entities;

public class ResponsePage {

	private Message message;
	private String description;

	public ResponsePage() {
		// TODO Auto-generated constructor stub
	}

	public ResponsePage(Message message, String description) {
		super();
		this.message = message;
		this.description = description;
	}

	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "ResponsePage [message=" + message + ", description=" + description + "]";
	}


}
