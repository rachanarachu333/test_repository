package com.capstone.app.entities;

public class Stock {
	private String product_name;
	private float stock;
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public float getStock() {
		return stock;
	}
	public void setStock(float stock) {
		this.stock = stock;
	}
	public Stock() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Stock(String product_name, float stock) {
		super();
		this.product_name = product_name;
		this.stock = stock;
	}
	@Override
	public String toString() {
		return "Stock [product_name=" + product_name + ", stock=" + stock + "]";
	}

}
