package com.capstone.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capstone.app.entities.Products;
import com.capstone.app.repositories.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;


	public List<Products> getProducts(){

		return (List<Products>) this.productRepository.findAll();
	}

	public boolean addProducts(Products products){
		this.productRepository.save(products);
		return true;
	}

	public boolean updateProduct(Products products) {

//		List<Products> listOfProducts = this.productRepository.findByManufacturer(products.getManufacturer());
//
//		for(Products pro : listOfProducts) {
//
//			System.out.println(pro.getProduct_name());
//			if(pro.getProduct_name().equalsIgnoreCase(products.getProduct_name())) {
//				this.productRepository.save(products);
//				return true;
//			}
//		}
		return false;

	}

	public boolean deleteProduct(Products product) {
		if(this.productRepository.existsById(product.getProduct_id())) {
			
			
			System.out.println(product.getProduct_id());
			
			this.productRepository.deleteById(product.getProduct_id());
			return true;
		}
		return false;
	}
}
