package com.capstone.app.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class LoginUser {

	@Id
	@Column(columnDefinition = "varchar(30)")
	private String email;

	@Column(columnDefinition = "varchar(20)")
	private String name;

	@Column(columnDefinition = "varchar(10)")
	private String password;

	@Column(columnDefinition = "varchar(20)")
	private String city;

	@Column(columnDefinition = "varchar(10)")
	private String mobile;
	
//	@OneToMany(mappedBy = "loginusers")
//	private List<_Order> orders=new ArrayList<>();

	public LoginUser() {
		System.out.println("Login User Class Constructor");
	}



	public LoginUser(String email, String name, String password, String city, String mobile) {
		super();
		this.email = email;
		this.name = name;
		this.password = password;
		this.city = city;
		this.mobile = mobile;
	}

//		public List<_Order> getOrders() {
//			return orders;
//		}
//	
//	
//	
//		public void setOrders(List<_Order> orders) {
//			this.orders = orders;
//		}



	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}



	@Override
	public String toString() {
		return "LoginUser [email=" + email + ", name=" + name + ", password=" + password + ", city=" + city
				+ ", mobile=" + mobile + "]";
	}




}//package com.capstone.app.entities;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.ManyToMany;
//
//@Entity
//public class LoginUser {
//	
//	@Id
//	@Column(columnDefinition = "varchar(30)")
//	private String email;
//	
//	@Column(columnDefinition = "varchar(20)")
//	private String name;
//	
//	@Column(columnDefinition = "varchar(10)")
//	private String password;
//	
//	@Column(columnDefinition = "varchar(20)")
//	private String city;
//	
//	@Column(columnDefinition = "varchar(10)")
//	private String mobile;
//	@ManyToMany
//	private List<Products> products=new ArrayList<>();
//	
//	
//	public LoginUser() {
//		System.out.println("Login User Class Constructor");
//	}
//
//	
//
//	public LoginUser(String email, String name, String password, String city, String mobile) {
//		super();
//		this.email = email;
//		this.name = name;
//		this.password = password;
//		this.city = city;
//		this.mobile = mobile;
//	}
//
//	public List<Products> getProducts() {
//		return products;
//	}
//
//
//
//	public void setProducts(List<Products> products) {
//		this.products = products;
//	}
//
//
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//	
//	public String getCity() {
//		return city;
//	}
//
//	public void setCity(String city) {
//		this.city = city;
//	}
//
//	public String getMobile() {
//		return mobile;
//	}
//
//	public void setMobile(String mobile) {
//		this.mobile = mobile;
//	}
//
//
//
//	@Override
//	public String toString() {
//		return "LoginUser [email=" + email + ", name=" + name + ", password=" + password + ", city=" + city
//				+ ", mobile=" + mobile + ", products=" + products + "]";
//	}
//
//
//
//	
//
//	
//
//	
//}