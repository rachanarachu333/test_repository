package com.capstone.app.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Cart {

	@Id
	@Column(columnDefinition = "varchar(10)")
	private String cart_id;


	@ManyToMany(mappedBy ="cart", cascade=CascadeType.ALL)
	private List<Products> products =new ArrayList<Products>();
    
	@OneToOne()
	@JoinColumn(name = "users")
	private LoginUser users;
	public Cart() {

	}

	

	public Cart(String cart_id) {
		super();
		this.cart_id = cart_id;
		
	}



	public LoginUser getUsers() {
		return users;
	}



	public void setUsers(LoginUser users) {
		this.users = users;
	}



	public String getCart_id() {
		return cart_id;
	}



	public void setCart_id(String cart_id) {
		this.cart_id = cart_id;
	}



	public List<Products> getProducts() {
		return products;
	}



	public void setProducts(List<Products> products) {
		this.products = products;
	}



	@Override
	public String toString() {
		return "Cart [cart_id=" + cart_id + ", products=" + products + "]";
	}



	
}//package com.capstone.app.entities;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.OneToMany;
//import javax.persistence.OneToOne;
//
//@Entity
//public class Cart {
//
//	@Id
//	@Column(columnDefinition = "varchar(10)")
//	private String cart_id;
//
//
//	@OneToMany(mappedBy ="cart", cascade=CascadeType.ALL)
//	private List<Products> products =new ArrayList<Products>();
//    
//	public Cart() {
//
//	}
//
//	
//
//	public Cart(String cart_id) {
//		super();
//		this.cart_id = cart_id;
//		
//	}
//
//
//
//	public String getCart_id() {
//		return cart_id;
//	}
//
//
//
//	public void setCart_id(String cart_id) {
//		this.cart_id = cart_id;
//	}
//
//
//
//	public List<Products> getProducts() {
//		return products;
//	}
//
//
//
//	public void setProducts(List<Products> products) {
//		this.products = products;
//	}
//
//
//
//	@Override
//	public String toString() {
//		return "Cart [cart_id=" + cart_id + ", products=" + products + "]";
//	}
//
//
//
//	
//}