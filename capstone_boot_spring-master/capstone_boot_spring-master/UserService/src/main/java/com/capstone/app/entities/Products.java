package com.capstone.app.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Products {
	
	@Id
	@Column(columnDefinition = "int(10)")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int product_id;
	
	@Column(columnDefinition = "varchar(20)")
	private String product_name;
	
	@Column(columnDefinition = "varchar(100)")
	private String manufacturer;
	
	@Column(columnDefinition = "float(10)")
	private float price;
	
	@Column(columnDefinition = "varchar(100)")
	private String description;

	@Column(columnDefinition = "varchar(40)")
	private String img_url;
	

	@Column(columnDefinition = "int(10)")
	private float stock;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="cart")
	private  List<Cart> cart=new ArrayList<>();
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="category")
	private Category category;
	
//	@ManyToMany(mappedBy = "product",cascade=CascadeType.ALL)
//	private List<_Order> order=new ArrayList<_Order>();
	
	public Products() {
		
	}
	
	public Products(int product_id, String product_name, String manufacturer, float price, String description,
			String img_url, float stock, List<Cart> cart, Category category) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
		this.manufacturer = manufacturer;
		this.price = price;
		this.description = description;
		this.img_url = img_url;
		this.stock = stock;
		this.cart = cart;
		this.category = category;
	}
	
	public String getImg_url() {
		return img_url;
	}

	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getStock() {
		return stock;
	}

	public void setStock(float stock) {
		this.stock = stock;
	}

	public List<Cart> getCart() {
		return cart;
	}

	public void setCart(List<Cart> cart) {
		this.cart = cart;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

//	public List<_Order> getOrder() {
//		return order;
//	}
//
//	public void setOrder(List<_Order> order) {
//		this.order = order;
//	}

	@Override
	public String toString() {
		return "Products [product_id=" + product_id + ", product_name=" + product_name + ", manufacturer="
				+ manufacturer + ", price=" + price + ", description=" + description + ", img_url=" + img_url
				+ ", stock=" + stock + ", cart=" + cart + ", category=" + category + "]";
	}
	
}