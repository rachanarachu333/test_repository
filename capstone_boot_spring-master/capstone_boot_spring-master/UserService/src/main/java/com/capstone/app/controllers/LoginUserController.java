package com.capstone.app.controllers;


import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.capstone.app.entities.LoginUser;
import com.capstone.app.entities.Message;
import com.capstone.app.entities.Products;
import com.capstone.app.entities.ResponsePage;
import com.capstone.app.entities.Stock;
import com.capstone.app.service.LoginUserService;
import com.capstone.app.service.ProductService;


@RestController
@CrossOrigin
public class LoginUserController {

	@Autowired
	private LoginUserService loginUserService;

	@Autowired
	private ProductService productService;



	@Autowired ServletContext context;

	@GetMapping(value="/getImages")
	@CrossOrigin
	public ResponseEntity<List<String>> getImages(){
		List<String> images = new ArrayList<String>();
		String filespath =context.getRealPath("images");
		File fileFolder = new File(filespath);
		if(fileFolder !=null) {
			for(final File file : fileFolder.listFiles()) {
				if(!file.isDirectory()) {
					String encodeBase64 = null;
					try{ 
						String extension =FilenameUtils.getExtension(file.getName());
						FileInputStream fileInputStream =new FileInputStream(file);
						byte[] bytes=new byte[(int)file.length()];
						fileInputStream.read(bytes);
						encodeBase64 = Base64.getEncoder().encodeToString(bytes);
						images.add("data:image/"+extension+";base64,"+encodeBase64);
						fileInputStream.close();

					}catch (Exception e) {
						// TODO: handle exception
					}
				}
			}
		}
		return new ResponseEntity<List<String>>(images,HttpStatus.OK);
	}






	@GetMapping("/stock")
	public ResponseEntity<List<Stock>> getStock(){

		//		System.out.println(products);

		List<Products> listOfProducts = this.productService.getProducts();
		List<Stock> stocklist=new ArrayList<>();
		for(Products productvar:listOfProducts) {
			Stock stockobj=new Stock(productvar.getProduct_name(),productvar.getStock());
			stocklist.add(stockobj);

		}

		return new ResponseEntity<List<Stock>>(stocklist,HttpStatus.OK);

	}

	@PostMapping("/post")
	public ResponseEntity<?> registered(@RequestBody LoginUser loginUser) {

		System.out.println(loginUser);
		boolean isInserted = false;

		try {

			isInserted = this.loginUserService.registerUser(loginUser);

		}catch(Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Regigstration Failed"));
		}

		if(isInserted) {
			return ResponseEntity.ok().body(new ResponsePage(Message.SUCCESS,"Registration Successfull"));
		}else {
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"You Are Already Registered Please Login"));
		}
	}

	@PostMapping("/loggedin")
	public ResponseEntity<?> login(@RequestBody LoginUser loginUser) {
		System.out.println(loginUser);

		try {
			boolean isPresent = this.loginUserService.findUser(loginUser);
			if(!isPresent) {
				return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Login Failed"));
			}else {
				return ResponseEntity.ok().body(new ResponsePage(Message.SUCCESS,"Login Successfull"));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Login Failed"));
		}
	}

	@PostMapping("/deleteuser")
	public ResponseEntity<?> deleteUser(@RequestBody LoginUser loginUser) {

		System.out.println(loginUser);

		try {
			boolean isDeleted = this.loginUserService.deleteUser(loginUser);
			if(isDeleted) {
				return ResponseEntity.ok().body(new ResponsePage(Message.SUCCESS,"Deletion SuccessFull"));
			}else {
				return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Deletion Failed"));
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Deletion Failed"));
		}

	}

	@GetMapping("/getUsers")
	public List<LoginUser> getUsers() {

		List<LoginUser> listOfUsers = this.loginUserService.getAllUsers();

		return listOfUsers;

	}


	@GetMapping("/products")
	public List<Products> getProducts() {

		List<Products> listOfProducts = this.productService.getProducts();

		//		for(Products product : listOfProducts) {
		//			System.out.println(product);
		//		}

		return listOfProducts;

	}



	@PostMapping("/addproduct")
	public ResponseEntity<?> addProduct(@RequestBody Products products) {

		System.out.println(products);

		boolean isAdded = false;

		try {

			isAdded = this.productService.addProducts(products);

		}catch(Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Add Product Operation Failed"));
		}

		if(isAdded) {
			return ResponseEntity.ok().body(new ResponsePage(Message.SUCCESS,"Product Added Successfully"));
		}else {
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Product is Already Present in the Database"));
		}

	}


	@PostMapping("/updateproduct")
	public ResponseEntity<?> updateProduct(@RequestBody Products products) {

		System.out.println(products);

		boolean isupdated = false;

		try {

			isupdated = this.productService.updateProduct(products);

		}catch(Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Update Product Operation Failed"));
		}

		if(isupdated) {
			return ResponseEntity.ok().body(new ResponsePage(Message.SUCCESS,"Product Updated Successfully"));
		}else {
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Product Not Found in the Database"));
		}

	}

	@PostMapping("/deleteproduct")
	public ResponseEntity<?> deleteUser(@RequestBody Products product) {

		boolean isDeleted = false;

		try {

			isDeleted = this.productService.deleteProduct(product);

		}catch(Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Delete Product Operation Failed"));
		}

		if(isDeleted) {
			return ResponseEntity.ok().body(new ResponsePage(Message.SUCCESS,"Product Delete Successfully"));
		}else {
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Product Not Found in the Database"));
		}

	}

}
