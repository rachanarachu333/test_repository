package com.capstone.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capstone.app.entities.LoginUser;
import com.capstone.app.repositories.LoginUserRepository;

@Service
public class LoginUserService {
	
	@Autowired
	private LoginUserRepository loginUserRepository;
	
	public boolean registerUser(LoginUser loginUser) {
		System.out.println(loginUser.getEmail());
		if(this.loginUserRepository.existsById(loginUser.getEmail())) {
			return false;
		}
		this.loginUserRepository.save(loginUser);
		return true;
	}
	
	public boolean findUser(LoginUser loginUser) throws Exception {
		System.out.println("LoginUser is " + loginUser);
		System.out.println(this.loginUserRepository.findById(loginUser.getEmail()).get());
		return this.loginUserRepository.existsById(loginUser.getEmail());
	}
	
	public boolean deleteUser(LoginUser loginUser) throws Exception {
		if(this.loginUserRepository.existsById(loginUser.getEmail())) {
			this.loginUserRepository.deleteById(loginUser.getEmail());
			return true;
		}
		return false;
		
	}

	public List<LoginUser> getAllUsers() {
		
		return (List<LoginUser>) this.loginUserRepository.findAll();
	}
}

