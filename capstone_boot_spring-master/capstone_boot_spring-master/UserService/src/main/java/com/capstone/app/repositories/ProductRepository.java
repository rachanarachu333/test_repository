package com.capstone.app.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.capstone.app.entities.Products;

@Repository
public interface ProductRepository extends CrudRepository<Products, Integer> {
	
	@Query(value = "delete from products where product_id =:id",nativeQuery=true)
	public boolean delete(@Param("id") int product_id);
	
}