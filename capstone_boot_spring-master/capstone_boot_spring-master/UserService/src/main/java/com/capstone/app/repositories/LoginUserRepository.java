package com.capstone.app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.capstone.app.entities.LoginUser;

@Repository
public interface LoginUserRepository extends CrudRepository<LoginUser, String>{

}

