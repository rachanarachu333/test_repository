package com.capstone.app.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Category {
	@Id
	private int category_id;
	
	private String category_name;
	
	@JsonManagedReference
	@OneToMany(mappedBy="category",cascade = CascadeType.ALL)
	private List<Products> products = new ArrayList<>();

	public Category() {
		
	}

	public Category(int category_id, String category_name, List<Products> products) {
		super();
		this.category_id = category_id;
		this.category_name = category_name;
		this.products = products;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public List<Products> getProducts() {
		return products;
	}

	public void setProducts(List<Products> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "Category [category_id=" + category_id + ", category_name=" + category_name + ", products=" + products
				+ "]";
	}
	
	
}//package com.capstone.app.entities;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.OneToMany;
//
//@Entity
//public class Category {
//	@Id
//	private int category_id;
//	
//	private String category_name;
//	
//	@OneToMany(mappedBy="category",cascade = CascadeType.ALL)
//	private List<Products> products = new ArrayList<>();
//
//	public Category() {
//		
//	}
//
//	public Category(int category_id, String category_name, List<Products> products) {
//		super();
//		this.category_id = category_id;
//		this.category_name = category_name;
//		this.products = products;
//	}
//
//	public int getCategory_id() {
//		return category_id;
//	}
//
//	public void setCategory_id(int category_id) {
//		this.category_id = category_id;
//	}
//
//	public String getCategory_name() {
//		return category_name;
//	}
//
//	public void setCategory_name(String category_name) {
//		this.category_name = category_name;
//	}
//
//	public List<Products> getProducts() {
//		return products;
//	}
//
//	public void setProducts(List<Products> products) {
//		this.products = products;
//	}
//
//	@Override
//	public String toString() {
//		return "Category [category_id=" + category_id + ", category_name=" + category_name + ", products=" + products
//				+ "]";
//	}
//	
//	
//}