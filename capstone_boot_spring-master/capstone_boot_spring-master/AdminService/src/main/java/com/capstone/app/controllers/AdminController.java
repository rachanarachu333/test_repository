package com.capstone.app.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.capstone.app.entities.Admin;
import com.capstone.app.entities.Message;
import com.capstone.app.entities.ResponsePage;
import com.capstone.app.entities.Stock;
import com.capstone.app.service.EmailSenderService;
import com.capstone.app.service.LoginUserService;

@RestController
@CrossOrigin()
public class AdminController {


	@Autowired
	private LoginUserService loginUserService;

	@Autowired
	private EmailSenderService emailservice;

	@Autowired
	private RestTemplate template;


	@EventListener(ApplicationReadyEvent.class)
	public void triggerMail() {
		String url = "http://USER-SERVICE/stock";

		String prod = "";

		ResponseEntity<Stock[]> listOfProductEntity = this.template.getForEntity(url, Stock[].class);

		List<Stock> listofstock = Arrays.asList(((HttpEntity<Stock[]>) listOfProductEntity).getBody());
		System.out.println(listofstock);

		if(listofstock.isEmpty()) {

			System.out.println("product list is empty");

		}else {

			for(Stock stock : listofstock) {

				System.out.println(stock);

				if(stock.getStock() < 10) {
    
					prod = prod + stock.getProduct_name()+", ";


				}
			}
			if(!prod.equals("")) {

				emailservice.sendSimpleEmail("kumardevendramishra10@gmail.com",
						"Hey, the product "+ prod +" is out of stock. please do the needful",
						"out of stock");	

			}
		}

	}








	@PostMapping("/getRegister")
	public ResponseEntity<?> register(@RequestBody Admin loginUser) {

		System.out.println(loginUser);

		boolean isInserted = this.loginUserService.registerUser(loginUser);
		if(isInserted) {
			return ResponseEntity.ok().body(new ResponsePage(Message.SUCCESS,"Registration Successfull"));
		}else {
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Regigstration Failed"));
		}

	}

	//	loginAdmin

	@PostMapping("/loginAdmin")
	public ResponseEntity<?> login(@RequestBody Admin loginUser) {

		System.out.println(loginUser);

		boolean isInserted = false;
		try {
			isInserted = this.loginUserService.findUser(loginUser);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Login Failed"));
		}
		if(isInserted) {
			return ResponseEntity.ok().body(new ResponsePage(Message.SUCCESS,"Login Successfull"));
		}else {
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Login Failed"));
		}

	}

	//	deleteuser
	@PostMapping("/deleteuser")
	public ResponseEntity<?> deleteUser(@RequestBody Admin loginUser) {

		System.out.println(loginUser);

		boolean isDeleted = false;
		try {
			isDeleted = this.loginUserService.deleteUser(loginUser);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Deletion Failed"));
		}
		if(isDeleted) {
			return ResponseEntity.ok().body(new ResponsePage(Message.SUCCESS,"Deletion Successfull"));
		}else {
			return ResponseEntity.badRequest().body(new ResponsePage(Message.FAILURE,"Deletion Failed"));
		}

	}
	


}













//	//**********************************Book Operations***********************************
//	
////	@RequestMapping("/books")
////	public boolean getBooks(Map<String, List<Book>> map) {
//
////		String url = "http://USER-SERVICE/book";
//
////		BookList adminBookList =  this.template.getForObject(url, BookList.class);
////		System.out.println(adminBookList.getListOfBooks());
//
////		map.put("listOfAllBooks", adminBookList.getListOfBooks());
//
////		if(adminBookList.getListOfBooks().isEmpty()) {
//			//			session.setAttribute("mssg", "List is Empty");
////			System.out.println("List is empty");
////			return false;
////		}else {
////			for(Book book : adminBookList.getListOfBooks()) {
////				System.out.println(book);
////			}
////			System.out.println("Login page");
////			return true;
////		}
////	}
//
//	@PostMapping("/registered")
//	public boolean registerUserDetails(@RequestBody LoginUser loginUser) {
//
//		boolean isInserted = this.loginUserService.registerUser(loginUser);
//		System.out.println(isInserted);
//
//		if(isInserted) {
//			System.out.println(loginUser);
//			return true;
//		}else {
//			return false;
//		}
//
//	}
//
//	@PostMapping("/users")
//	public LoginUser getUserDetails(@RequestBody LoginUser loginUser) throws Exception {
//
//		LoginUser user = this.loginUserService.findUser(loginUser);
//
//		System.out.println(user);
//
//		return user;
//	}
//	
//	@PostMapping("/add")
//	public boolean addBook(@RequestBody Book book) {
//		
//		return this.bookService.addBookDetails(book);
//	}
//	
//	@PostMapping("/update")
//	public boolean updateBook(@RequestBody Book book) {
//		
//		return this.bookService.updateBook(book);
//	}
//	
//	@PostMapping("/delete")
//	public boolean deleteBook(@RequestBody Book book) {
//		
//		return this.bookService.deleteBook(book.getBookId());
//	}
//	
//	@GetMapping("/book")
//	public ResponseEntity<List<Book>> getAllBooks() {
//		
//		List<Book> listOfBooks = this.bookService.getAllBooks();
//		
//		return new ResponseEntity<List<Book>>(listOfBooks,HttpStatus.OK);
//	}
//	
//	
//	
//	//******************************User Operations*********************************
//	
//	@PostMapping("/adduser")
//	public boolean addUser(@RequestBody LoginUser loginUser) {
//		
//		return this.loginUserService.registerUser(loginUser);
//	}
//	
//	@PostMapping("/deleteuser")
//	public boolean deleteUser(@RequestBody LoginUser loginUser) {
//		return this.loginUserService.deleteUser(loginUser);
//	}
//	
//	@PostMapping("/updateuser")
//	public boolean updateUser(@RequestBody LoginUser loginUser) {
//		return this.loginUserService.updateUser(loginUser);
//	}
//	
//	@GetMapping("/allusers")
//	public ResponseEntity<List<LoginUser>> getAllUsers() {
//		
//		List<LoginUser> listOfUsers = this.loginUserService.getAllUsers();
//		
//		return new ResponseEntity<List<LoginUser>>(listOfUsers,HttpStatus.OK);
//	}
//}
