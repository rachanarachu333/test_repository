package com.capstone.app.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.capstone.app.entities.Admin;


@Repository
public interface LoginUserRepository extends CrudRepository<Admin, String>{
	
}
