package com.capstone.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {
	
	@Id
	@Column(columnDefinition = "varchar(30)")
	private String email;
	
	@Column(columnDefinition = "varchar(20)")
	private String name;
	
	@Column(columnDefinition = "varchar(10)")
	private String password;
	
	@Column(columnDefinition = "varchar(20)")
	private String city;
	
	@Column(columnDefinition = "varchar(10)")
	private String mobile;
	
	public Admin() {
		System.out.println("Login User Class Constructor");
	}

	public Admin(String email, String name, String password,String city,String mobile) {
		super();
		this.email = email;
		this.name = name;
		this.password = password;
		this.city = city;
		this.mobile=mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return "LoginUser [email=" + email + ", name=" + name + ", password=" + password + ", city=" + city
				+ ", mobile=" + mobile + "]";
	}

	
}