package com.capstone.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capstone.app.entities.Admin;
import com.capstone.app.repositories.LoginUserRepository;

@Service
public class LoginUserService {
	
	@Autowired
	private LoginUserRepository loginUserRepository;
	
	public boolean registerUser(Admin loginUser) {
		System.out.println(loginUser.getEmail());
		if(this.loginUserRepository.existsById(loginUser.getEmail())) {
			return false;
		}
		this.loginUserRepository.save(loginUser);
		return true;
	}
	
	public boolean findUser(Admin loginUser) throws Exception {
		System.out.println("LoginUser is " + loginUser);
		System.out.println(this.loginUserRepository.findById(loginUser.getEmail()).get());
		return this.loginUserRepository.existsById(loginUser.getEmail());
	}
	
	public boolean deleteUser(Admin loginUser) {
		System.out.println(loginUser.getEmail());
		if(this.loginUserRepository.existsById(loginUser.getEmail())) {
			this.loginUserRepository.deleteById(loginUser.getEmail());
			return true;
		}
		return false;
	}

	public boolean updateUser(Admin loginUser) {
		if(this.loginUserRepository.existsById(loginUser.getEmail())) {
			this.loginUserRepository.save(loginUser);
			return true;
		}
		return false;
	}

	
}
